package jefferyemanuel.org.githubDemo.usecases.base;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;


public abstract class BaseUseCase {

    abstract Observable buildUseCaseObservable();

    public Observable buildObservable() {
        return this.buildUseCaseObservable()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread());
    }
}
