package jefferyemanuel.org.githubDemo.usecases.base;


import java.io.IOException;

import io.reactivex.Observer;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.Disposable;
import retrofit2.HttpException;
import timber.log.Timber;



public class DefaultSubscriber<T> implements Observer<T> {

    Disposable disposable;

    @Override
    public void onSubscribe(@NonNull Disposable d) {
        Timber.d("subscriber called now ...");
        disposable = d;
    }

    @Override
    public void onNext(@NonNull T t) {
        Timber.d("onNext called now ..." + t);
    }

    @Override
    public void onError(@NonNull Throwable e) {
        Timber.e(e);
        Timber.d("onError called now ...");
        if (e instanceof HttpException) {
            //todo analytics can go here
            // We had non-2XX http error
            HttpException exception = (HttpException) e;
            if (exception.code() == 401) {
                //maybe token expired here, we can do some action,im not sure the lifetime of weather map token. if its forever, ignore
            }
        }
        if (e instanceof IOException) {
            // A network or conversion error happened
        }

    }

    @Override
    public void onComplete() {
        Timber.d("onComplete called now ...");
    }

    public void unsubscribe() {
        Timber.d("Un-subscribed called now ..");
        if (disposable != null && !disposable.isDisposed()) {
            disposable.dispose();
        }
    }
}