package jefferyemanuel.org.githubDemo.ui.application

import jefferyemanuel.org.githubDemo.dagger.components.DaggerTestAppComponent
import jefferyemanuel.org.githubDemo.dagger.components.TestAppComponent
import jefferyemanuel.org.githubDemo.dagger.modules.ActivityModule
import jefferyemanuel.org.githubDemo.dagger.modules.TestAppModule
import jefferyemanuel.org.githubDemo.dagger.modules.TestNetworkModule

/**
 * Created by j2emanue on 5/20/17.
 */

class WeatherTestApplication : WeatherApplication() {


    override val appComponent: TestAppComponent by lazy {
        DaggerTestAppComponent
                .builder()
                .appModule(TestAppModule(this))
                .activityModule(ActivityModule())
                .testNetworkModule(TestNetworkModule())
                .build()
    }
}
