package jefferyemanuel.org.githubDemo.ui.application

import android.app.Activity
import android.app.Application
import android.content.pm.ActivityInfo
import android.os.Bundle
import java.util.*

class WeatherMapActivityLifecycleCallbacks : Application.ActivityLifecycleCallbacks {

    private var orientationPortraitExceptionsList: ArrayList<String>? = null

    init {
        initOrientationPortraitExceptions()
    }

    //add component names of activities that are restricted to portrait orientation only
    private fun initOrientationPortraitExceptions() {
        orientationPortraitExceptionsList = ArrayList()
        orientationPortraitExceptionsList!!.add("FQN OF ANY ACTIVITY THAT SHOULD BE ALLOWED TO CHANGE ORIENTATION CAN GO HERE")
    }

    override fun onActivityCreated(activity: Activity, savedInstanceState: Bundle?) {
        if (activity != null && !isPortraitExempt(activity)) {
            activity.requestedOrientation = ActivityInfo.SCREEN_ORIENTATION_PORTRAIT
        }
    }

    private fun isPortraitExempt(activity: Activity?): Boolean {
        return !(activity == null || activity.componentName == null) && orientationPortraitExceptionsList!!.contains(activity.componentName.className)
    }


    override fun onActivityStarted(activity: Activity) {

    }

    override fun onActivityResumed(activity: Activity) {

    }

    override fun onActivityPaused(activity: Activity) {

    }

    override fun onActivityStopped(activity: Activity) {

    }

    override fun onActivitySaveInstanceState(activity: Activity, outState: Bundle) {

    }

    override fun onActivityDestroyed(activity: Activity) {

    }
}
