package jefferyemanuel.org.githubDemo.ui.mvp.base;


import android.os.Bundle;
import android.support.annotation.StringRes;

import com.hannesdorfmann.mosby.mvp.MvpView;




public interface BaseMvpView extends MvpView {

    void showError(@StringRes int e);

    void showError(String msg);

    void showLoading(boolean show);

    void showLoading(String tag, boolean show);

    void showLoading(boolean b, boolean cancelable);

    void proceedToActivity(Class aClass, Bundle b, boolean clearTask, boolean finish);

    void exit();
}
