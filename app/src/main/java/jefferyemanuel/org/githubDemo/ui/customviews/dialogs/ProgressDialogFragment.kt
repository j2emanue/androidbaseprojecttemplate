package jefferyemanuel.org.githubDemo.ui.customviews.dialogs

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v4.app.FragmentManager
import dmax.dialog.SpotsDialog
import jefferyemanuel.org.githubDemo.R
import timber.log.Timber

class ProgressDialogFragment : DialogFragment() {

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return SpotsDialog(activity, R.style.customspinner_style)
    }

    override fun show(manager: FragmentManager, tag: String) {

        try {
            val ft = manager.beginTransaction()
            ft.add(this, tag)
            ft.commit()
        } catch (e: IllegalStateException) {
            Timber.d(e)
        }

    }

    companion object {

        private val DIALOG_CANCELABLE = false

        @JvmOverloads
        fun newInstance(cancelable: Boolean = DIALOG_CANCELABLE): ProgressDialogFragment {
            val args = Bundle()
            val progressDialogFragment = ProgressDialogFragment()
            progressDialogFragment.arguments = args
            progressDialogFragment.isCancelable = cancelable
            return progressDialogFragment
        }
    }
}
