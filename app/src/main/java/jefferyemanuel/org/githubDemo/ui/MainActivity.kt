package jefferyemanuel.org.githubDemo.ui

import android.os.Bundle
import jefferyemanuel.org.githubDemo.R
import jefferyemanuel.org.githubDemo.ui.application.WeatherApplication
import jefferyemanuel.org.githubDemo.ui.base.WeatherMapBaseActivity
import jefferyemanuel.org.githubDemo.ui.mvp.WeatherMapPresenter
import jefferyemanuel.org.githubDemo.ui.mvp.contracts.WeatherView.WeatherMapView
import javax.inject.Inject

class MainActivity : WeatherMapBaseActivity<WeatherMapView, WeatherMapPresenter>(), WeatherMapView {

    @Inject
    lateinit var mPresenter: WeatherMapPresenter

    override fun createPresenter(): WeatherMapPresenter {
        return mPresenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        (applicationContext as WeatherApplication).appComponent.inject(this)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
