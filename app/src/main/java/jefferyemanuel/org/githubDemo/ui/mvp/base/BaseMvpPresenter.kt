package jefferyemanuel.org.githubDemo.ui.mvp.base

import com.hannesdorfmann.mosby.mvp.MvpBasePresenter
import jefferyemanuel.org.githubDemo.R


public abstract class BaseMvpPresenter<V : BaseMvpView> : MvpBasePresenter<V>() {

    fun showError() {
        view!!.showLoading(false)
        view!!.showError(R.string.ERROR)
    }

    fun showError(msg: String) {
        view!!.showLoading(false)
        view!!.showError(msg)
    }

    fun showLoading(tag: String, show: Boolean) {
        view!!.showLoading(tag, show)
    }

    fun showLoading(show: Boolean) {
        view!!.showLoading(show)
    }

    fun showLoading(show: Boolean, cancelable: Boolean) {
        view!!.showLoading(show, cancelable)
    }

}