package jefferyemanuel.org.githubDemo.ui.mvp



import jefferyemanuel.org.githubDemo.ui.mvp.base.BaseMvpPresenter
import jefferyemanuel.org.githubDemo.ui.mvp.contracts.WeatherView
import javax.inject.Inject

class WeatherMapPresenter @Inject constructor(): BaseMvpPresenter<WeatherView.WeatherMapView>()
