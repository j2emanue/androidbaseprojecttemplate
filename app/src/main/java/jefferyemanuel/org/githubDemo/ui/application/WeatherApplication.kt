package jefferyemanuel.org.githubDemo.ui.application

import android.app.Application
import com.facebook.common.logging.FLog
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.imagepipeline.core.ImagePipelineConfig
import com.facebook.imagepipeline.listener.RequestListener
import com.facebook.imagepipeline.listener.RequestLoggingListener
import com.facebook.stetho.Stetho
import jefferyemanuel.org.githubDemo.dagger.components.AppComponent
import jefferyemanuel.org.githubDemo.dagger.components.DaggerAppComponent
import jefferyemanuel.org.githubDemo.dagger.modules.ActivityModule
import jefferyemanuel.org.githubDemo.dagger.modules.AppModule
import jefferyemanuel.org.githubDemo.dagger.modules.NetworkModule
import java.util.*


open class WeatherApplication : Application() {

    open val appComponent: AppComponent by lazy {
        DaggerAppComponent
                .builder()
                .appModule(AppModule(this)).activityModule(ActivityModule())
                .networkModule(NetworkModule())
                .build()
    }

    lateinit var mActivityLifecycleCallbacks: WeatherMapActivityLifecycleCallbacks

    override fun onCreate() {
        super.onCreate()
        appComponent.inject(this)
        initStetho()
        initializeFresco()
        // Register to be informed of activities starting up
        mActivityLifecycleCallbacks = WeatherMapActivityLifecycleCallbacks()
        registerActivityLifecycleCallbacks(mActivityLifecycleCallbacks)

    }

    private fun initStetho() {
        Stetho.initializeWithDefaults(this)
    }

    private fun initializeFresco() {
        val requestListeners = HashSet<RequestListener>()
        requestListeners.add(RequestLoggingListener())
        val config = ImagePipelineConfig.newBuilder(this)
                .setRequestListeners(requestListeners)
                .build()
        Fresco.initialize(this, config)
        FLog.setMinimumLoggingLevel(FLog.VERBOSE)
    }
}