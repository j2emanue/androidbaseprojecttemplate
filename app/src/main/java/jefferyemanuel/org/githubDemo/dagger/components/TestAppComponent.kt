package jefferyemanuel.org.githubDemo.dagger.components

import dagger.Component
import jefferyemanuel.org.githubDemo.dagger.modules.ActivityModule
import jefferyemanuel.org.githubDemo.dagger.modules.AppModule
import jefferyemanuel.org.githubDemo.dagger.modules.TestNetworkModule
import jefferyemanuel.org.githubDemo.data.repositories.WeatherMapSharedPrefRepo
import javax.inject.Singleton


@Singleton
@Component(modules = arrayOf(AppModule::class, TestNetworkModule::class, ActivityModule::class))
interface TestAppComponent : AppComponent {
    fun httpClient(): okhttp3.OkHttpClient
    fun sharedPrefs(): WeatherMapSharedPrefRepo
}