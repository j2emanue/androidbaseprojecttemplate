package jefferyemanuel.org.githubDemo.dagger.modules;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import jefferyemanuel.org.githubDemo.dagger.modules.qualifiers.ApplicationContext;

@Module
public class AppModule {

    public static final String UNSECURE_PREFS = "unsecuredSharedPrefs";

    private Application application;

    public AppModule(Application application) {
        this.application = application;
    }

    @Provides
    @Singleton
    @ApplicationContext
    public Context provideContext() {
        return application;
    }

    @Singleton
    @Provides
    public SharedPreferences provideSharedPreferences(@ApplicationContext Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    @Singleton
    @Provides
    @Named(UNSECURE_PREFS)
    public SharedPreferences provideUnsecureSharedPreferences(@ApplicationContext Context context) {
        //useful if we want to keep some shared preferences after a logout, for example. we can clear defaults but keep items that are not secure items
        return context.getSharedPreferences("unsecureDefaults", Context.MODE_PRIVATE);
    }
}
