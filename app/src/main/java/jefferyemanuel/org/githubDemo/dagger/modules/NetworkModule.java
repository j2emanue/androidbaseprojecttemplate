package jefferyemanuel.org.githubDemo.dagger.modules;

import android.content.Context;
import android.net.Uri;
import android.support.annotation.NonNull;

import com.facebook.stetho.okhttp3.StethoInterceptor;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.ihsanbal.logging.Level;
import com.ihsanbal.logging.LoggingInterceptor;
import com.squareup.picasso.Picasso;

import java.util.concurrent.TimeUnit;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import jefferyemanuel.org.githubDemo.BuildConfig;
import jefferyemanuel.org.githubDemo.dagger.modules.qualifiers.ApplicationContext;
import jefferyemanuel.org.githubDemo.retrofit.interceptors.ApplicationInterceptor;
import okhttp3.OkHttpClient;
import okhttp3.internal.platform.Platform;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import timber.log.Timber;


@Module
public class NetworkModule {

    public static final String BASE_API_URL = "WEATHER_MAP_BASE_URL";

    @Provides
    @Named(BASE_API_URL)
    public String provideBaseUrlString() {
        return BuildConfig.BASE_ENDPOINT;
    }

    @NonNull
    @Provides
    @Singleton
    public Converter.Factory provideGsonConverter() {

        final Gson Gson = new GsonBuilder()
                .setPrettyPrinting()
                .create();
        return GsonConverterFactory.create(Gson);
    }




    @NonNull
    @Singleton
    @Provides
    public OkHttpClient provideOkHttpClient(@NonNull ApplicationInterceptor applicationInterceptor) {
        final OkHttpClient.Builder builder = new OkHttpClient.Builder();

         builder.addInterceptor(applicationInterceptor);
        if (BuildConfig.RETROFIT_LOG_ALL) {

            //****** lets use pretty logging only
            LoggingInterceptor prettyRetrofitLogInterceptor = new LoggingInterceptor.Builder()
                    .loggable(BuildConfig.DEBUG)
                    .setLevel(Level.BASIC)
                    .log(Platform.INFO)
                    .request("Weather map API Request")
                    .response("Weather map  API Response")
                    .build();

            builder.addInterceptor(prettyRetrofitLogInterceptor);
            builder.addNetworkInterceptor(new StethoInterceptor());
        }

        builder.readTimeout(20, TimeUnit.SECONDS);
        builder.connectTimeout(20, TimeUnit.SECONDS);
        return builder.build();
    }

    @NonNull
    @Provides
    @Singleton
    public Retrofit provideRetrofit(@NonNull Converter.Factory converter, @NonNull OkHttpClient client, @Named(BASE_API_URL) String baseUrl) {
        return new Retrofit.Builder()
                .baseUrl(baseUrl)
                .client(client)
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .addConverterFactory(converter)
                .build();
    }

    @Provides
    @Singleton //todo might be better to use newSingleThreadExecutor() if scoped singleton

    public Picasso providePicasso(@NonNull @ApplicationContext Context context, OkHttpClient okHttpClient) {
        //Picasso picasso = Picasso.with(context);

        Picasso.Builder builder = new Picasso.Builder(context);
        if (BuildConfig.DEBUG) {
            builder.loggingEnabled(true);
            builder.indicatorsEnabled(false);

            builder.listener(new Picasso.Listener() {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, @NonNull Exception exception) {
                    Timber.e(exception);
                    exception.printStackTrace();
                }
            });
        }
        Picasso picasso = builder.build();
        Picasso.setSingletonInstance(picasso);
        return picasso;
    }
}
