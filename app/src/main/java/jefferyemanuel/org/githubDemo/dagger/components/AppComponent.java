package jefferyemanuel.org.githubDemo.dagger.components;


import javax.inject.Singleton;

import dagger.Component;
import jefferyemanuel.org.githubDemo.dagger.modules.ActivityModule;
import jefferyemanuel.org.githubDemo.dagger.modules.AppModule;
import jefferyemanuel.org.githubDemo.ui.MainActivity;
import jefferyemanuel.org.githubDemo.ui.application.WeatherApplication;



@Singleton
@Component(modules = {AppModule.class, jefferyemanuel.org.githubDemo.dagger.modules.NetworkModule.class, ActivityModule.class})
public interface AppComponent {

    void inject(WeatherApplication target);

    void inject(MainActivity target);





}