package jefferyemanuel.org.githubDemo.retrofit.interceptors;

import java.io.IOException;

import javax.inject.Inject;

import jefferyemanuel.org.githubDemo.BuildConfig;
import jefferyemanuel.org.githubDemo.data.repositories.WeatherMapSharedPrefRepo;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okio.Buffer;


public class ApplicationInterceptor implements Interceptor {
    WeatherMapSharedPrefRepo sharedPrefRepo;

    @Inject
    public ApplicationInterceptor(WeatherMapSharedPrefRepo sharedPrefRepo) {
        this.sharedPrefRepo = sharedPrefRepo;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {

        Request original = chain.request();
        HttpUrl originalHttpUrl = original.url();

        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("appid", BuildConfig.OPEN_WEATHER_API_KEY)
                .build();

        Request.Builder requestBuilder = original.newBuilder()
                .url(url);

        Request request = requestBuilder.build();
        logRequestsToFabric(request);

        return chain.proceed(request);
    }

    private void logRequestsToFabric(Request request) {

       //here i am showing you a very useful way to log to crashlytics all network requests. great for troubleshooting
        try {
          //  Crashlytics.log("request:" + request.toString());
          //  Crashlytics.log("body:" + bodyToString(request.body()));
        } catch (Exception e) {
        }
    }

    private String bodyToString(final RequestBody body) {
        try {
            final RequestBody copy = body;
            final Buffer buffer = new Buffer();
            if (copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        } catch (final IOException e) {
            return "did not work";
        }
    }
}
