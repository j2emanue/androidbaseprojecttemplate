package jefferyemanuel.org.githubDemo.retrofit.Api;

import android.support.annotation.NonNull;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface WeatherApi {

    @NonNull
    @GET("weather url")
    @Headers({"Content-Type:application/json"})
    Observable<Object> getWeather();

}