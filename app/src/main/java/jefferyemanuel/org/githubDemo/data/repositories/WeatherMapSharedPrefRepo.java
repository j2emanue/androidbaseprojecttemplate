package jefferyemanuel.org.githubDemo.data.repositories;

import android.content.SharedPreferences;

import java.util.Map;

import javax.inject.Inject;
import javax.inject.Named;

import static jefferyemanuel.org.githubDemo.dagger.modules.AppModule.UNSECURE_PREFS;



public class WeatherMapSharedPrefRepo {


    private final String DEFAULT = "";

    SharedPreferences sp;

    @Named(UNSECURE_PREFS)
    SharedPreferences unsecure_sp;

    @Inject
    public WeatherMapSharedPrefRepo(SharedPreferences sp, SharedPreferences unsecure_sp) {
        this.sp = sp;
        this.unsecure_sp = unsecure_sp;
    }

    public void clearAll() {

        sp.edit().clear().apply();
    }

    public Map<String, ?> getAllPreferences() {
        return sp.getAll();
    }

}
