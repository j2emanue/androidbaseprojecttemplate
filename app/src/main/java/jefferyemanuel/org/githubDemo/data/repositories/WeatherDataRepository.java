package jefferyemanuel.org.githubDemo.data.repositories;

import javax.inject.Inject;

import retrofit2.Retrofit;



public class WeatherDataRepository {

    private final Retrofit mRetrofit;

    @Inject
    public WeatherDataRepository(Retrofit retrofit) {
        mRetrofit = retrofit;
    }
}